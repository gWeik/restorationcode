clc;clear;

InputPathImages = 'D:\GiulioWeikmann\Sentinel-2\Processed\Images\';
InputPathMasks  = 'D:\GiulioWeikmann\Sentinel-2\Processed\CloudMask\';

S2_img   = dir([InputPathImages '*.tif']);
S2_masks = dir([InputPathMasks '*.tif']);

counter = zeros(size(S2_img,1),1);

for i = 1:size(S2_img,1)
    disp(i);
    mask = imread([InputPathMasks S2_masks(i).name]);
    mask(mask==1) = 0;
    mask(or(or(or(mask==8,mask==7),mask==9),mask==10)) = 1;
    mask(mask~=1) = 0;
    mask = mask == 1;
    if sum(sum(mask)) > size(mask,1)*size(mask,2)*0.75
        counter(i) = 1;
    end
end
save('D:\GiulioWeikmann\Sentinel-2\ClearImmInd','counter');