clc;clear;%close all;
%==========================================================
% Import Data
%==========================================================

% mapID       = importdata('mapID.mat');%single(imread('LabeledNDVICrops_01_CzechRepublic.tif'));            
% imgTarget   = single(imread('Original_10m\S2A_L2A_20170521T100031_N0205_R122_T33UXQ_20170521T100029_10m.tif'));      % cloudy image
% cMaskTarget = ~single(imread('S2A_L2A_20170521T100031_N0205_R122_T33UXQ_20170521T100029_CloudMask.tif'));  % clouds have values 1


% imgTarget   = importdata('D:\GiulioWeikmann\RestorationCode\Output\Imm1.mat');
% cMaskTarget = importdata('D:\GiulioWeikmann\RestorationCode\Output\1.mat');  % clouds have values 1


listM = dir(['D:\GiulioWeikmann\RestorationCode\Output\Masks\' '*mat']);
listI = dir(['D:\GiulioWeikmann\Sentinel-2\Processed\Images\' '*tif']);
clearImmInd = importdata('D:\GiulioWeikmann\Sentinel-2\ClearImmInd.mat');
ImmInd = find(clearImmInd==0);
ImmInd = ImmInd(1:size(listM,1));

immRef = importdata('D:\GiulioWeikmann\Sentinel-2\ImmRef.mat');
immRefind = find(immRef==1);
% immRefind = immRefind(1:4);

for i = 1:size(ImmInd,1)
imgTarget = imread([listI(ImmInd(i)).folder '\' listI(ImmInd(i)).name]);
cMaskTarget = importdata([listM(i).folder '\' num2str(ImmInd(i)) '.mat']);

minim = min(abs(immRefind-i));
selref = find(abs(immRefind-i)==minim);
if minim == 0
    selref = selref + 1;
end

imgRef    = imread([listI(ImmInd(immRefind(selref(1)))).folder '\' listI(ImmInd(immRefind(selref(1)))).name]);         % clear image 1
cMaskRef   = importdata([listM(1).folder '\' num2str(ImmInd(immRefind(selref(1)))) '.mat']);

k_Region = 10;

%==========================================================
% Remove Outliers
%==========================================================
% imgTarget = DetectMinMax(imgTarget, cMaskTarget);
% imgRef_1  = DetectMinMax(imgRef_1, cMaskRef);
% imgRef_2  = DetectMinMax(imgRef_2, cMaskRef);
% imgRef_3  = DetectMinMax(imgRef_3, cMaskRef);
% imgRef_4  = DetectMinMax(imgRef_4, cMaskRef);
% % imgRef_5  = DetectMinMax(imgRef_5, cMaskRef);
% % imgRef_6  = DetectMinMax(imgRef_6, cMaskRef);
% % imgRef_7  = DetectMinMax(imgRef_7, cMaskRef);
% % imgRef_8  = DetectMinMax(imgRef_8, cMaskRef);


%==========================================================
% Calculate NDVI
%==========================================================
b2 = 4;
b1 = 3;


NDVI_ts    = (imgRef(:,:,b2) - imgRef(:,:,b1)) ./ (imgRef(:,:,b2) + imgRef(:,:,b1));                 % clear image 1
% NDVI_5    = single(imread('Images\NDVI-tiff\S2A_L2A_20170111T100351_N0204_R122_T33UXQ_20170111T100351_NDVI_B0804.tif'));                   % clear image 4
% NDVI_6    = single(imread('Images\NDVI-tiff\S2A_L2A_20170329T095021_N0204_R079_T33UXQ_20170329T095024_NDVI_B0804.tif'));
% NDVI_7    = single(imread('Images\NDVI-tiff\S2A_L2A_20170401T100021_N0204_R122_T33UXQ_20170401T100022_NDVI_B0804.tif'));% clear image 4
% NDVI_8    = single(imread('Images\NDVI-tiff\S2A_L2A_20170511T100031_N0205_R122_T33UXQ_20170511T100539_NDVI_B0804.tif'));% clear image 4

%
NDVI_ts(isnan(NDVI_ts)) = 0;

%==========================================================
% Mask Processing
%==========================================================

se = strel('disk',2);
se1 = strel('disk',2);
cMaskTarget = bwareaopen(cMaskTarget,100);
cMaskTarget = imdilate(cMaskTarget,se);
cMaskRef = bwareaopen(cMaskRef,100);
cMaskRef = imdilate(cMaskRef,se1);
cMaskMultiTemp = or( cMaskTarget~=0, cMaskRef~=0 );
imgTarget_est = imgTarget;

%==========================================================
% Restore Image
%==========================================================

nl = 5000;
nc = 5000;

n_tile_x = ceil(size(imgTarget,1)/nl);
n_tile_y = ceil(size(imgTarget,2)/nc);


for i_tile_y =  1% : n_tile_y
      
    for i_tile_x =  1% : n_tile_x
    
            %% Cut Image in tile region

            l1 = nl*(i_tile_x-1)+1;
            c1 = nc*(i_tile_y-1)+1;
            
            Rmax = min( l1+nl-1, size(imgTarget,1) );
            Rmin = max( l1, 1 );
            Cmax = min( c1+nc-1 , size(imgTarget,2) );
            Cmin = max( c1, 1);

            nl_cut = Rmax - Rmin +1 ;
            nc_cut = Cmax - Cmin +1 ;
            nb = size(imgTarget,3);
            
            %% Multittemporal CLustering

            NDVI_ts_Vec    = reshape( NDVI_ts( Rmin:Rmax, Cmin:Cmax, : ) , [nl_cut* nc_cut, size(NDVI_ts,3)]) ; 
            cMaskRefVec    = reshape( cMaskRef( Rmin:Rmax, Cmin:Cmax ), [nl_cut* nc_cut, 1] ) ;
            cMaskTargetVec = reshape( cMaskTarget( Rmin:Rmax, Cmin:Cmax ), [nl_cut* nc_cut, 1] ) ;

%             x = multithresh(NDVI_ts_Vec( cMaskRefVec==0,:),k_Region);
            

% coeff = pca(imgRefVec);
% temp = imgRefVec*coeff(:,1:3);
            label1 = histeq(NDVI_ts_Vec( cMaskRefVec==0,:),10);
%             label1 = histeq(normalize(imgRefVec(:,1), 'zscore'),10);
            t = unique(label1);
            label1(label1 == 1) = size(t,1);
            for i_label = 1:size(t,1)-1
                label1(label1 == t(i_label)) = i_label;
            end
%             label1 = label1+1;

%             label1 = discretize(NDVI_ts_Vec( cMaskRefVec==0,:),k_Region);
%             label1 = kmeans(label1,k_Region);

            LabelKmean = zeros(size(cMaskRefVec,1),1);
            LabelKmean(cMaskRefVec == 0) = label1;
            labelclouds = LabelKmean( cMaskTargetVec == 1 );
%             counter = unique(labelclouds);
            counter = unique(nonzeros(labelclouds));
            
            %% Convert to Vector
            multiMaskTempVec  = reshape( cMaskMultiTemp( Rmin:Rmax, Cmin:Cmax ) , [nl_cut* nc_cut, 1]) ; 
            TargetVec         = reshape( imgTarget   ( Rmin:Rmax , Cmin:Cmax , : ),    [nl_cut*nc_cut,nb] ); 
            imgRefVec         = reshape( imgRef( Rmin:Rmax , Cmin:Cmax , : ), [nl_cut*nc_cut,size(imgRef,3)] );             
            PixUnderCloudRef  = imgRefVec(( cMaskTargetVec == 1 ),: );         
            PixUnderCloudTarg = TargetVec((cMaskTargetVec == 1),:);
            NDVI_ts_UnderCloudRef = NDVI_ts_Vec(( cMaskTargetVec == 1 ),: );   
            
            
             for i_cluster = 1 : size(counter,1)
                    disp(i_cluster);
                    samples_under_cloud_cluster = PixUnderCloudRef( labelclouds==counter(i_cluster), : );
                    sel = LabelKmean == counter(i_cluster);
                    sel(multiMaskTempVec) = 0;
                    
                    if sum(unique(sel)) ~= 0
                        img_t2_label_sel = imgRefVec(sel,:);
                        img_t1_label_sel = TargetVec(sel,:); 
       
                        n = knnsearch( NDVI_ts_Vec(sel,:) , NDVI_ts_UnderCloudRef(labelclouds==counter(i_cluster),:), 'K', 1 ); %ref
                        n = unique(n);
                        
                        idx = randsample(size(n,1),size(n,1));
                        
                        inputs_1  = img_t2_label_sel( n(idx(1:round(size(n,1)/2))), : );
                        targets_1 = img_t1_label_sel( n(idx(1:round(size(n,1)/2))), : );
                        
                        inputs_2  = img_t2_label_sel( n(idx(round(size(n,1)/2)+1:end)), : );
                        targets_2 = img_t1_label_sel( n(idx(round(size(n,1)/2)+1:end )), : );
                        
                        
                         b = regress(targets_1(:,1),[ones(size(inputs_1,1),1) inputs_1 inputs_1.^2 ]);
                         predicted_label = b'*[ones(size(targets_2,1),1) inputs_2 inputs_2.^2  ]';
                         mse1 = (predicted_label'-targets_2(:,1)).^2;
                         [msesorted1,I1] = sort(mse1);
                         
                         b = regress(targets_2(:,1),[ones(size(inputs_2,1),1) inputs_2 inputs_2.^2 ]);
                         predicted_label = b'*[ones(size(targets_1,1),1) inputs_1 inputs_1.^2  ]';
                         mse2 = (predicted_label'-targets_1(:,1)).^2;
                         [msesorted2,I2] = sort(mse2);

                         mmse = quantile([msesorted1;msesorted2],0.3);
                         I2(msesorted2 > mmse) = [];
                         I1(msesorted1 > mmse) = [];
%                          I2 = I2 + round(size(n,1)/2)+1;
                         
%                          idx = randsample(size(n,1),min(size(n,1),2000));
                        idx = randsample([idx(I1);idx(I2)],min(2000,size([idx(I1);idx(I2)],1)));
                      
                        inputs  = img_t2_label_sel( n(idx), : );
                        targets = img_t1_label_sel( n(idx), : );
                        for bb = 1:nb
                             
%                             X = inputs;
%                             Y = targets(:,bb);
%                             Mdl = TreeBagger(50,X,Y,'OOBPrediction','On','Method','regression');
%                             predicted_label = predict(Mdl,samples_under_cloud_cluster);
                            
                            b = regress(targets(:,bb),[ones(size(inputs,1),1) inputs inputs.^2 ]);
                            predicted_label = b'*[ones(size(samples_under_cloud_cluster,1),1) samples_under_cloud_cluster samples_under_cloud_cluster.^2  ]';
                            predicted_label = predicted_label';
                            PixUnderCloudTarg( labelclouds==counter(i_cluster),bb) = predicted_label;
                            
                        end
                    end
             end
            TargetVec(cMaskTargetVec==1,:) = PixUnderCloudTarg;
            imgTarget_est( Rmin:Rmax, Cmin:Cmax, :)  = reshape(TargetVec, [nl_cut,nc_cut,nb]);

    end
end


%% Show Results
imgTarget = single(imgTarget);
% rgb  = cat(3, imgTarget(:,:,3), imgTarget(:,:,2) ,imgTarget(:,:,1));
% rgb2 = cat(3, imgRef(:,:,3), imgRef(:,:,2) ,imgRef(:,:,1));
rgb3 = cat(3, imgTarget_est(:,:,3), imgTarget_est(:,:,2) ,imgTarget_est(:,:,1));

%% Plot
%  edge = imdilate(xor(imdilate(cMaskTarget,strel('disk',7)),imerode(imdilate(cMaskTarget,strel('disk',7)),strel('disk',1))),strel('disk',1));
% 
%             for b=1:3
%                 app  = rgb3(:,:,b);
%                 app2 = rgb (:,:,b);
%                 if b==1
%                     app(edge)  = 1e4;
%                     app2(edge) = 1e4;
%                 else 
%                     app(edge)  = 0;
%                     app2(edge) = 0;
%                 end
%                 rgb3(:,:,b) = app;
%                 rgb (:,:,b) = app2;
%             end
%             
% figure(),
% ax1 = subplot(1,2,1); imshow(rgb3(1:Rmax,1:Cmax,:)*3/1e4);title('BagTree');
% ax2 = subplot(1,2,2); imshow(rgb(1:Rmax,1:Cmax,:)*3/1e4);
% linkaxes([ax1,ax2]);

save(['D:\GiulioWeikmann\RestorationCode\Output\Ensemble\' num2str(ImmInd(i))],'rgb3');

end

