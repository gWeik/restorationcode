



for i=1:3
    band = img(:,:,i);
     maxVal = quantile(band(:),0.95);
     minVal = quantile(band(:),0.001);
     
     band(band>maxVal)=maxVal;
     band(band<minVal)=minVal;

    
    img2(:,:,i) =( band -minVal)  ./(maxVal - minVal);%normalize(band,'range');
end

[~, T, ~] =  imreducehaze(img2(:,:,[3 2 1]),'Method','approxdcp');
figure(4),imshow(img2(:,:,[3 2 1])*2.* (T>quantile(T(:),0.7)));


