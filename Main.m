clc;clear

InputPathImages = 'D:\GiulioWeikmann\Sentinel-2/Images\';
InputPathMasks  = 'D:\GiulioWeikmann\Sentinel-2/CloudMasks\';

S2_img   = dir([InputPathImages '*.tif']);
S2_masks = dir([InputPathMasks '*.tif']);

%========================================================================
% Import Data
%========================================================================

img = imread([S2_img(1,1).folder '/' S2_img(1,1).name]);
mask = imread([S2_masks(1,1).folder '/' S2_masks(1,1).name]);
[nl,nc,~] = size(img);

TS_B     = zeros( nl*nc, size(S2_img,1) );
TS_R      = zeros( nl*nc, size(S2_img,1) );
TS_G    = zeros( nl*nc, size(S2_img,1) );
TS_IR = zeros( nl*nc, size(S2_img,1) );

TS_masks    = zeros( nl*nc, size(S2_img,1) );

TS_B(:,1) = reshape( img(:,:,1), [nl*nc,1]);
TS_R(:,1)  = reshape( img(:,:,2), [nl*nc,1]);
TS_G(:,1) = reshape( img(:,:,3), [nl*nc,1]);
TS_IR(:,1) = reshape( img(:,:,4), [nl*nc,1]);

TS_masks(:,1)    = reshape( mask, [nl*nc,1]);

    for i = 2 : size(S2_img,1)
        figure,imshow(img(:,:,[3 2 1])*4/1e4)

        img = imread([S2_img(i,1).folder '/' S2_img(i,1).name]);    
        TS_B(:,i) = reshape( img(:,:,1), [nl*nc,1]);
        TS_R(:,i)  = reshape( img(:,:,2), [nl*nc,1]);
        TS_G(:,i) = reshape( img(:,:,3), [nl*nc,1]);
        TS_IR(:,i) = reshape( img(:,:,4), [nl*nc,1]);

        mask = imread([S2_masks(i,1).folder '/' S2_masks(i,1).name]);
        TS_masks(:,i) = reshape( mask, [nl*nc,1]);

    end
  
    
tile    
clear img mask 
%========================================================================
% Temporal Trend Analysis
%========================================================================
cloud_Th = quantile(TS_B,0.9,2);
Mask_New = zeros(size(TS_B));
Mask_New(TS_B>cloud_Th)=1;

    for i = 1 : size(Mask_New,2)
        Check = reshape(Mask_New(:,i),[nl,nc]);
        figure(i*100),imshow(Check)
    end
    
tile 

%========================================================================
% Extract Noisy Training Set (AGGIUNGERE ALTRE BANDE)
%========================================================================
CloudSen2Cor = and(TS_masks>7,TS_masks<11);
ClearSen2Cor = and(TS_masks>3,TS_masks<7);

CloudClass =  [ TS_B(and(Mask_New==1,CloudSen2Cor==1)), ...
                TS_R(and(Mask_New==1,CloudSen2Cor==1)), ...
                TS_G(and(Mask_New==1,CloudSen2Cor==1)), ...
                TS_IR(and(Mask_New==1,CloudSen2Cor==1)) ];
ClearClass =  [ TS_B(and(Mask_New==0,ClearSen2Cor==1)),...
                TS_R(and(Mask_New==0,ClearSen2Cor==1)), ...
                TS_G(and(Mask_New==0,ClearSen2Cor==1)), ...
                TS_IR(and(Mask_New==0,ClearSen2Cor==1))];
ShadowClass = [ TS_B(TS_masks==3),...
                TS_R(TS_masks==3), ...
                TS_G(TS_masks==3), ...
                TS_IR(TS_masks==3) ];


idx = randsample( size(CloudClass,1), min(size(CloudClass,1), 1e4 ));
TR{1,1} = CloudClass(idx,:);

idx = randsample( size(ClearClass,1), min(size(ClearClass,1), 1e4 ));
TR{2,1} = ClearClass(idx,:);

idx = randsample( size(ShadowClass,1), min(size(ShadowClass,1), 1e4 ));
TR{3,1} = ShadowClass(idx,:);

     
%========================================================================
% Extract Core of the Distribution
%========================================================================
classDistribution = cell(3,1);
alpha = 0.9; % lower values  more conservative

    for iii = 1:3
        
        omega = TR{iii,1};
        d2= 1;
        x=0;
        while sum(d2>x) ~=0  
            gm = fitgmdist(omega,1,'RegularizationValue',0.1);
            d2 = mahal(gm,omega);
            x = chi2inv( alpha , 2*size(omega,2) );
            omega(d2>x,:)=[];
        end
        label = ones( size( omega,1 ) ,1) * iii;
        classDistribution{iii,1} =   omega ;

    end

%========================================================================
% Ensemble
%========================================================================
X = [classDistribution{1,1};classDistribution{2,1};classDistribution{3,1}];
Y = [ones(size(classDistribution{1,1},1),1);ones(size(classDistribution{2,1},1),1)*2;ones(size(classDistribution{3,1},1),1)*3];

Mdl  = fitcensemble(X,Y,'Method','AdaBoostM2','NumLearningCycles',10);
predMeanX = predict(Mdl,[TS_B(:,5),TS_R(:,5),TS_G(:,5),TS_IR(:,5)]);
MaskCloud = reshape(predMeanX,[nl,nc]);

img = imread([S2_img(5,1).folder '/' S2_img(5,1).name]); 
rgb = img(:,:,[3 2 1])*4/1e4;
rgb_masked = rgb.*(MaskCloud~=1);
figure,
ax1 = subplot(2,2,1);imshow(rgb);
ax2 = subplot(2,2,2);imagesc(MaskCloud),daspect([1,1,1]);
ax3 = subplot(2,2,3);imagesc(rgb_masked),daspect([1,1,1]);
linkaxes([ax1,ax2,ax3])
