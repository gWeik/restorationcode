clc;clear;close all;

InputPathImages = 'D:\GiulioWeikmann\Sentinel-2/Images\';
InputPathMasks  = 'D:\GiulioWeikmann\Sentinel-2/CloudMasks\';

addpath('functions');

S2_img   = dir([InputPathImages '*.tif']);
S2_masks = dir([InputPathMasks '*.tif']);

%========================================================================
% Import Data
%========================================================================

img = imread([S2_img(1,1).folder '/' S2_img(1,1).name]);
mask = imread([S2_masks(1,1).folder '/' S2_masks(1,1).name]);
[nl,nc,~] = size(img);

clearImmInd = importdata('D:\GiulioWeikmann\RestorationCode_v1\datas/clearImmInd.mat');

n_imm = 10; %size(S2_img,1)
clearImmInd = clearImmInd(1:n_imm);
% CloudImages = find( clearImmInd == 1 );
ImmInd = find(clearImmInd==0);

TS_B    = zeros( nl*nc, size(ImmInd,1) );
TS_G    = zeros( nl*nc, size(ImmInd,1) );
TS_R    = zeros( nl*nc, size(ImmInd,1) );
TS_IR   = zeros( nl*nc, size(ImmInd,1) );
Haze    = zeros( nl*nc, size(ImmInd,1) );

TS_masks    = zeros( nl*nc, size(ImmInd,1) );

TS_B(:,1)    = reshape( img(:,:,1), [nl*nc,1]);
TS_G(:,1)    = reshape( img(:,:,2), [nl*nc,1]);
TS_R(:,1)    = reshape( img(:,:,3), [nl*nc,1]);
TS_IR(:,1)   = reshape( img(:,:,4), [nl*nc,1]);
Haze(:,1)    = reshape( HazeDetector_f(img), [nl*nc,1]);

TS_masks(:,1)    = reshape( mask, [nl*nc,1]);

    for i = 2:size(ImmInd,1)    %: n_imm %size(S2_img,1)
%         figure,imshow(img(:,:,[3 2 1])*4/1e4)

        img = imread([S2_img(ImmInd(i),1).folder '/' S2_img(ImmInd(i),1).name]);   
        TS_B(:,i)    = reshape( img(:,:,1), [nl*nc,1]);
        TS_G(:,i)    = reshape( img(:,:,2), [nl*nc,1]);
        TS_R(:,i)    = reshape( img(:,:,3), [nl*nc,1]);
        TS_IR(:,i)   = reshape( img(:,:,4), [nl*nc,1]);
        mask = imread([S2_masks(ImmInd(i),1).folder '/' S2_masks(ImmInd(i),1).name]);
        TS_masks(:,i) = reshape( mask, [nl*nc,1]);
        Haze(:,i)     = reshape( HazeDetector_f(img), [nl*nc,1]);

        
    end
    
% tile    
clear img mask 


% TS_B(:,CloudImages)     = [];
% TS_R(:,CloudImages)     = [];
% TS_G(:,CloudImages)     = [];
% TS_IR(:,CloudImages)    = [];
% TS_masks(:,CloudImages) = [];
% Haze(:,CloudImages)     = [];

%========================================================================
% Plot Haze
%========================================================================
for i = 1:size(ImmInd,1)
    img = imread([S2_img(ImmInd(i),1).folder '/' S2_img(ImmInd(i),1).name]);   
    figure(i),imshow(img(:,:,[3 2 1])*4/1e4 .* (reshape(Haze(:,i),[5004,5010])>quantile(Haze(:,i),0.7)));
end
tile

%========================================================================
% Temporal Trend Analysis
%========================================================================
cloud_Th = quantile(TS_B,0.85,2);
var_Th = var(TS_B,1,2);

Mask_New = zeros(size(TS_B));
Mask_New(TS_B>cloud_Th)=1;
Q = 0.4;
Mask_New(var_Th<quantile(var_Th,Q),:) = 0;

SinglePixels = zeros(size(Mask_New));
    for i = 1 : size(Mask_New,2)
        Check = reshape(bwareaopen(reshape(Mask_New(:,i),[nl,nc]),50),[nl*nc,1]);
        SinglePixels(:,i) = xor(Check,Mask_New(:,i));
        Mask_New(:,i) = Check;

%         img = imread([S2_img(ImmInd(i),1).folder '/' S2_img(ImmInd(i),1).name]); 
%         prova = redClouds(img,find(SinglePixels(:,i) == 1));
%         prova2 = redClouds(img,find(Mask_New(:,i)==1));
    end

    
% diff = xor(Mask_New,Haze>quantile((quantile(Haze,0.7)),0.9));
diff = xor(Mask_New,Haze>max(quantile(Haze,0.7)));
% for i = 1 : size(Mask_New,2)
%     img = imread([S2_img(ImmInd(i),1).folder '/' S2_img(ImmInd(i),1).name]);   
% %     figure(i),imshow(img(:,:,[3 2 1])*4/1e4 .* reshape(diff(:,i),[5004,5010]));
%     figure(i),imshow(img(:,:,[3 2 1])*4/1e4 .* reshape(or(diff(:,i),and(Mask_New(:,i) == 1,CloudSen2Cor(:,i) == 1)),[nl,nc]));
% end
% tile

%========================================================================
% Extract Noisy Training Set (AGGIUNGERE ALTRE BANDE)
%========================================================================
CloudSen2Cor = and(TS_masks>7,TS_masks<11);
ClearSen2Cor = and(TS_masks>3,TS_masks<7);


CloudClass =  [ TS_B(or(diff,and(Mask_New    == 1,CloudSen2Cor == 1))), ...
                TS_R(or(diff,and(Mask_New    == 1,CloudSen2Cor == 1))), ...
                TS_G(or(diff,and(Mask_New    == 1,CloudSen2Cor == 1))), ...
                TS_IR(or(diff,and(Mask_New   == 1,CloudSen2Cor == 1)))];
ClearClass =  [ TS_B(and(Mask_New    == 0,ClearSen2Cor == 1)), ...
                TS_R(and(Mask_New    == 0,ClearSen2Cor ==1 )), ...
                TS_G(and(Mask_New    == 0,ClearSen2Cor == 1)), ...
                TS_IR(and(Mask_New   == 0,ClearSen2Cor == 1))];         
ShadowClass = [ TS_B(TS_masks    == 3), ...
                TS_R(TS_masks    == 3), ...
                TS_G(TS_masks    == 3), ...
                TS_IR(TS_masks   == 3)];
LPCloudsClass =  [ TS_B(SinglePixels == 1), ...
                TS_R(SinglePixels    == 1), ...
                TS_G(SinglePixels    == 1), ...
                TS_IR(SinglePixels   == 1)]; 

idx = randsample( size(CloudClass,1), min(size(CloudClass,1), 1e3 ));
TR{1,1} = CloudClass(idx,:);

idx = randsample( size(ClearClass,1), min(size(ClearClass,1), 1e3 ));
TR{2,1} = ClearClass(idx,:);

idx = randsample( size(ShadowClass,1), min(size(ShadowClass,1), 1e3 ));
TR{3,1} = ShadowClass(idx,:);

idx = randsample( size(LPCloudsClass,1), min(size(LPCloudsClass,1), 1e3 ));
TR{4,1} = LPCloudsClass(idx,:);

     
%========================================================================
% Extract Core of the Distribution
%========================================================================
classDistribution = cell(4,1);
alpha = 0.9; % lower values  more conservative

    for iii = 1:4
        
        omega = TR{iii,1};
        d2= 1;
        x=0;
        while sum(d2>x) ~=0  
            gm = fitgmdist(omega,1,'RegularizationValue',0.1);
            d2 = mahal(gm,omega);
            x = chi2inv( alpha , 2*size(omega,2) );
            omega(d2>x,:)=[];
        end
        label = ones( size( omega,1 ) ,1) * iii;
        classDistribution{iii,1} =   omega ;

    end

%========================================================================
% Ensemble
%========================================================================
X = [classDistribution{1,1};classDistribution{2,1};classDistribution{3,1};classDistribution{4,1}];
Y = [ones(size(classDistribution{1,1},1),1);ones(size(classDistribution{2,1},1),1)*2;ones(size(classDistribution{3,1},1),1)*3;ones(size(classDistribution{4,1},1),1)*4];

Mdl  = fitcensemble(X,Y,'Method','AdaBoostM2','NumLearningCycles',10);

indSel = 3;

predMeanX = predict(Mdl,[TS_B(:,indSel),TS_R(:,indSel),TS_G(:,indSel),TS_IR(:,indSel)]);
MaskCloud = reshape(predMeanX,[nl,nc]);

% ImmInd = find(clearImmInd==0);
Immind = ImmInd(indSel);

img = imread([S2_img(Immind,1).folder '/' S2_img(Immind,1).name]); 
rgb = img(:,:,[3 2 1])*4/1e4;
rgb_masked = rgb.*(MaskCloud~=1);
figure,
ax1 = subplot(2,2,1);imshow(rgb);
ax2 = subplot(2,2,2);imagesc(MaskCloud),daspect([1,1,1]);
ax3 = subplot(2,2,3);imagesc(rgb_masked),daspect([1,1,1]);
linkaxes([ax1,ax2,ax3])
[~,blueimm] = blueClouds(img,find(MaskCloud==4)); title(['VarQuantile' num2str(Q)]);
redClouds(img,find(MaskCloud==1)), title(['VarQuantile' num2str(Q)]);

close all;

figure(),
ax1 = subplot(1,2,1); imshow(blueimm(:,:,[3,2,1]));
ax2 = subplot(1,2,2);imagesc(rgb_masked),daspect([1,1,1]);
linkaxes([ax1,ax2]);

