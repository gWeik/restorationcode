clc;clear;close all;

% InputPathImages = 'D:\GiulioWeikmann\Sentinel-2/Images\';
InputPathImages = 'D:\GiulioWeikmann\Sentinel-2\Processed\Images\';
InputPathMasks  = 'D:\GiulioWeikmann\Sentinel-2\Processed\CloudMask\';

addpath('functions');

S2_img   = dir([InputPathImages '*.tif']);
S2_img = S2_img(52:end);
S2_masks = dir([InputPathMasks '*.tif']);
S2_masks = S2_masks(52:end);

%========================================================================
% Import Data
%========================================================================

img = imread([S2_img(1,1).folder '/' S2_img(1,1).name]);
mask = imread([S2_masks(1,1).folder '/' S2_masks(1,1).name]);
[nl,nc,~] = size(img);

clearImmInd = importdata('D:\GiulioWeikmann\Sentinel-2\ClearImmInd.mat');
clearImmInd = clearImmInd(52:end);

n_imm = 20; %size(S2_img,1)
clearImmInd = clearImmInd(1:n_imm);
% CloudImages = find( clearImmInd == 1 );
ImmInd = find(clearImmInd==0);

TS_B    = zeros( nl*nc, size(ImmInd,1) );
TS_G    = zeros( nl*nc, size(ImmInd,1) );
TS_R    = zeros( nl*nc, size(ImmInd,1) );
TS_11   = zeros( nl*nc, size(ImmInd,1) );
TS_12   = zeros( nl*nc, size(ImmInd,1) );
TS_IR   = zeros( nl*nc, size(ImmInd,1) );
Haze    = zeros( nl*nc, 10 );

TS_masks    = zeros( nl*nc, size(ImmInd,1) );

TS_B(:,1)    = reshape( img(:,:,1), [nl*nc,1]);
TS_G(:,1)    = reshape( img(:,:,2), [nl*nc,1]);
TS_R(:,1)    = reshape( img(:,:,3), [nl*nc,1]);
TS_IR(:,1)   = reshape( img(:,:,4), [nl*nc,1]);
TS_11(:,1)   = reshape( img(:,:,9), [nl*nc,1]);
TS_12(:,1)   = reshape( img(:,:,10), [nl*nc,1]);
% Haze(:,1)    = reshape( HazeDetector_f(img(:,:,[1,2,3])), [nl*nc,1]);

TS_masks(:,1)    = reshape( mask, [nl*nc,1]);

    for i = 2:size(ImmInd,1)    %: n_imm %size(S2_img,1)
%         figure,imshow(img(:,:,[3 2 1])*4/1e4)
        disp(i);
        img = imread([S2_img(ImmInd(i),1).folder '/' S2_img(ImmInd(i),1).name]);   
        TS_B(:,i)    = reshape( img(:,:,1), [nl*nc,1]);
        TS_G(:,i)    = reshape( img(:,:,2), [nl*nc,1]);
        TS_R(:,i)    = reshape( img(:,:,3), [nl*nc,1]);
        TS_IR(:,i)   = reshape( img(:,:,4), [nl*nc,1]);
        TS_11(:,i)   = reshape( img(:,:,9), [nl*nc,1]);
        TS_12(:,i)   = reshape( img(:,:,10), [nl*nc,1]);
        mask = imread([S2_masks(ImmInd(i),1).folder '/' S2_masks(ImmInd(i),1).name]);
        TS_masks(:,i) = reshape( mask, [nl*nc,1]);
%         if i < 10
%             Haze(:,i)     = reshape( HazeDetector_f(img(:,:,[1,2,3])), [nl*nc,1]);
%         end
        
    end
    
Haze = importdata('D:\GiulioWeikmann\PythonConv\SEOM\data\haze.mat');    
% tile    
clear img mask 

%%RESIZE
for i = 1:size(ImmInd,1)
    TS_Br(:,i) = reshape(imresize(reshape(TS_B(:,i),[5004,5010]),0.5),[6267510,1]);
    TS_Gr(:,i) = reshape(imresize(reshape(TS_G(:,i),[5004,5010]),0.5),[6267510,1]);
    TS_Rr(:,i) = reshape(imresize(reshape(TS_R(:,i),[5004,5010]),0.5),[6267510,1]);
    TS_IRr(:,i) = reshape(imresize(reshape(TS_IR(:,i),[5004,5010]),0.5),[6267510,1]);
    TS_11r(:,i) = reshape(imresize(reshape(TS_11(:,i),[5004,5010]),0.5),[6267510,1]);
    TS_12r(:,i) = reshape(imresize(reshape(TS_12(:,i),[5004,5010]),0.5),[6267510,1]);
    TS_masksr(:,i) = reshape(imresize(reshape(TS_masks(:,i),[5004,5010]),0.5),[6267510,1]);
    if i < 11
        Hazer(:,i) = reshape(imresize(reshape(Haze(:,i),[5004,5010]),0.5),[6267510,1]);
    end
end

%========================================================================
% Plot Haze
%========================================================================
% for i = 1:10
%     img = cat(3,reshape(TS_R(:,i),[5004,5010]),reshape(TS_G(:,i),[5004,5010]),reshape(TS_B(:,i),[5004,5010]));
%     figure(i),imshow(img(:,:,[1 2 3])*4/1e4 .* (reshape(Haze(:,i),[5004,5010])>quantile(Haze(:,i),0.9)));
% end
% tile

%========================================================================
% Temporal Trend Analysis
%========================================================================

cloud_Th = quantile(TS_Br,0.5,2);
Mask_New = zeros(size(TS_Br));
Mask_New(TS_Br>cloud_Th)=1;

Q = 0.4;
for i = 1:2
    i_m = 1 + 5*(i-1);
    var_Th = var(TS_Br(:,i_m:i_m+4),1,2);
    
    Mask_New_5 = Mask_New(:,i_m:i_m+4);
    Mask_New_5(var_Th<quantile(var_Th,Q),:) = 0;
    Mask_New(:,i_m:i_m+4) = Mask_New_5;
end


%========================================================================
% Plot Variance
%========================================================================
% for i = 1 : size(Mask_New,2)
%     img = cat(3,reshape(TS_Rr(:,i),[nl/2,nc/2]),reshape(TS_Gr(:,i),[nl/2,nc/2]),reshape(TS_Br(:,i),[nl/2,nc/2]));
%     figure(i), imshow(img*4/1e4 .* reshape(var_Th<quantile(var_Th,Q),[nl/2,nc/2]));
% end
% tile
close all;
lel = max(quantile(Hazer,0.6));
% for i = 1 : 10% size(Mask_New,2)
%     img = cat(3,reshape(TS_Rr(:,i),[nl/2,nc/2]),reshape(TS_Gr(:,i),[nl/2,nc/2]),reshape(TS_Br(:,i),[nl/2,nc/2]));
%     figure(i), imshow(img*4/1e4 .* reshape(Hazer(:,i)>lel,[nl/2,nc/2]));
% end
TestHaze = Hazer>lel;
tile

% for i = 1 : 10%size(Mask_New,2)
%     img = cat(3,reshape(TS_Rr(:,i),[nl/2,nc/2]),reshape(TS_Gr(:,i),[nl/2,nc/2]),reshape(TS_Br(:,i),[nl/2,nc/2]));
%     figure(i), imshow(img*4/1e4 .* reshape(Mask_New(:,i),[nl/2,nc/2]));
% end
% tile


for i = 1 : size(Mask_New,2)
    Mask_New(:,i) = reshape(bwareaopen(reshape(Mask_New(:,i),[nl/2,nc/2]),50),[6267510,1]);
end
    
%========================================================================
% Extract Noisy Training Set (AGGIUNGERE ALTRE BANDE)
%========================================================================
% CloudSen2Cor = and(TS_masks>7,TS_masks<11);
% ClearSen2Cor = and(TS_masks>3,TS_masks<7);

TS_Br = TS_Br(:,1:10);
TS_Rr = TS_Rr(:,1:10);
TS_Gr = TS_Gr(:,1:10);
TS_IRr = TS_IRr(:,1:10);
TS_11r = TS_11r(:,1:10);
TS_12r = TS_12r(:,1:10);
TS_B = TS_B(:,1:10);
TS_R = TS_R(:,1:10);
TS_G = TS_G(:,1:10);
TS_IR = TS_IR(:,1:10);
TS_11 = TS_11(:,1:10);
TS_12 = TS_12(:,1:10);


CloudClass =  [ TS_Br(TestHaze    == 1), ...
                TS_Rr(TestHaze    == 1), ...
                TS_Gr(TestHaze    == 1), ...
                TS_IRr(TestHaze   == 1), ...
                TS_11r(TestHaze   == 1), ...
                TS_12r(TestHaze   == 1)];
ClearClass =  [ TS_Br(Mask_New    == 0), ...
                TS_Rr(Mask_New    == 0), ...
                TS_Gr(Mask_New    == 0), ...
                TS_IRr(Mask_New   == 0), ...
                TS_11r(Mask_New    == 0), ...
                TS_12r(Mask_New    == 0)];   
ShadowClass = [ TS_Br(TS_masksr    == 3), ...
                TS_Rr(TS_masksr    == 3), ...
                TS_Gr(TS_masksr    == 3), ...
                TS_IRr(TS_masksr   == 3), ...
                TS_11r(TS_masksr    == 3), ...
                TS_12r(TS_masksr    == 3)];

idx = randsample( size(CloudClass,1),    min(size(CloudClass,1),    1e4 ));
TR{1,1} = CloudClass(idx,:);

idx = randsample( size(ClearClass,1),    min(size(ClearClass,1),    1e4 ));
TR{2,1} = ClearClass(idx,:);

idx = randsample( size(ShadowClass,1),   min(size(ShadowClass,1),   1e4 ));
TR{3,1} = ShadowClass(idx,:);

%========================================================================
% Extract Core of the Distribution
%========================================================================
classDistribution = cell(3,1);
% alpha = 0.9; % lower values  more conservative
% 
    for iii = 1:3
%         
        omega = TR{iii,1};
%         d2= 1;
%         x=0;
%         while sum(d2>x) ~=0  
%             gm = fitgmdist(omega,1,'RegularizationValue',0.1);
%             d2 = mahal(gm,omega);
%             x = chi2inv( alpha , 2*size(omega,2) );
%             omega(d2>x,:)=[];
%         end
%         label = ones( size( omega,1 ) ,1) * iii;
        classDistribution{iii,1} =   omega ;
% 
    end

%========================================================================
% Ensemble
%========================================================================
X = [classDistribution{1,1};classDistribution{2,1};classDistribution{3,1}];
Y = [ones(size(classDistribution{1,1},1),1);ones(size(classDistribution{2,1},1),1)*2;ones(size(classDistribution{3,1},1),1)*3];

Mdl  = fitcensemble(X,Y,'Method','AdaBoostM2','NumLearningCycles',10);

indSel = 2;

predMeanX = predict(Mdl,[TS_B(:,indSel),TS_R(:,indSel),TS_G(:,indSel),TS_IR(:,indSel),TS_11(:,indSel),TS_12(:,indSel)]);
MaskCloud = reshape(predMeanX,[nl,nc]);

% ImmInd = find(clearImmInd==0);
Immind = ImmInd(indSel);

img = imread([S2_img(Immind,1).folder '/' S2_img(Immind,1).name]); 
rgb = img(:,:,[3 2 1])*4/1e4;
rgb_masked = rgb.*(MaskCloud~=1);
[~,blueimm] = blueClouds(img,find(MaskCloud==1)); title(['VarQuantile' num2str(Q)]);
redClouds(img,find(MaskCloud==1)), title(['VarQuantile' num2str(Q)]);

close all;

figure(),
ax1 = subplot(1,2,1); imshow(blueimm(:,:,[3,2,1]));
ax2 = subplot(1,2,2);imagesc(rgb_masked),daspect([1,1,1]);
linkaxes([ax1,ax2]);

Mcloud = MaskCloud == 1;
Mcloud = bwareaopen(Mcloud,200);
Mcloud = imdilate(Mcloud,strel('disk',10));
Mcloud(and(Mcloud == 1, MaskCloud ==4)) = 1;
imshow(img(:,:,[3 2 1])*4/1e4 .* (Mcloud~=1));
