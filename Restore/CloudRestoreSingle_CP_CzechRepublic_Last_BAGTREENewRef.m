clc;clear;%close all;
%==========================================================
% Import Data
%==========================================================

% mapID       = importdata('mapID.mat');%single(imread('LabeledNDVICrops_01_CzechRepublic.tif'));            
% imgTarget   = single(imread('Original_10m\S2A_L2A_20170521T100031_N0205_R122_T33UXQ_20170521T100029_10m.tif'));      % cloudy image
% cMaskTarget = ~single(imread('S2A_L2A_20170521T100031_N0205_R122_T33UXQ_20170521T100029_CloudMask.tif'));  % clouds have values 1


imgTarget   = importdata('D:\GiulioWeikmann\RestorationCode\Output\Imm1.mat');
cMaskTarget = importdata('D:\GiulioWeikmann\RestorationCode\Output\1.mat');  % clouds have values 1


listM = dir(['D:\GiulioWeikmann\RestorationCode\Output\Masks\' '*mat']);
listI = dir(['D:\GiulioWeikmann\Sentinel-2\Processed\Images\' '*tif']);
clearImmInd = importdata('D:\GiulioWeikmann\Sentinel-2\ClearImmInd.mat');
ImmInd = find(clearImmInd==0);
ImmInd = ImmInd(1:size(listM,1));

immRef = importdata('D:\GiulioWeikmann\Sentinel-2\ImmRef.mat');
immRefind = find(immRef==1);
immRefind = immRefind(1:4);



imgRef_1    = imread([listI(ImmInd(immRefind(1))).folder '\' listI(ImmInd(immRefind(1))).name]);         % clear image 1
imgRef_2    = imread([listI(ImmInd(immRefind(1))).folder '\' listI(ImmInd(immRefind(2))).name]);          % clear image 2
imgRef_3    = imread([listI(ImmInd(immRefind(1))).folder '\' listI(ImmInd(immRefind(3))).name]);
imgRef_4    = imread([listI(ImmInd(immRefind(1))).folder '\' listI(ImmInd(immRefind(4))).name]);            % clear image 4
% imgRef_5    = single(imread('Original_10m\S2A_L2A_20170111T100351_N0204_R122_T33UXQ_20170111T100351_10m.tif'));                   % clear image 4
% imgRef_6    = single(imread('Original_10m\S2A_L2A_20170329T095021_N0204_R079_T33UXQ_20170329T095024_10m.tif'));                   % clear image 4
% imgRef_7    = single(imread('Original_10m\S2A_L2A_20170401T100021_N0204_R122_T33UXQ_20170401T100022_10m.tif'));                   % clear image 4
% imgRef_8    = single(imread('Original_10m\S2A_L2A_20170511T100031_N0205_R122_T33UXQ_20170511T100539_10m.tif'));                   % clear image 4
cMaskRef1   = importdata([listM(1).folder '\' num2str(ImmInd(immRefind(1))) '.mat']);
cMaskRef2   = importdata([listM(1).folder '\' num2str(ImmInd(immRefind(2))) '.mat']);
cMaskRef3   = importdata([listM(1).folder '\' num2str(ImmInd(immRefind(3))) '.mat']);
cMaskRef4   = importdata([listM(1).folder '\' num2str(ImmInd(immRefind(4))) '.mat']);

cMaskRef = or(or(or(cMaskRef1,cMaskRef2),cMaskRef3),cMaskRef4);


k_Region = 10;

%==========================================================
% Remove Outliers
%==========================================================
% imgTarget = DetectMinMax(imgTarget, cMaskTarget);
% imgRef_1  = DetectMinMax(imgRef_1, cMaskRef);
% imgRef_2  = DetectMinMax(imgRef_2, cMaskRef);
% imgRef_3  = DetectMinMax(imgRef_3, cMaskRef);
% imgRef_4  = DetectMinMax(imgRef_4, cMaskRef);
% % imgRef_5  = DetectMinMax(imgRef_5, cMaskRef);
% % imgRef_6  = DetectMinMax(imgRef_6, cMaskRef);
% % imgRef_7  = DetectMinMax(imgRef_7, cMaskRef);
% % imgRef_8  = DetectMinMax(imgRef_8, cMaskRef);

imgRef = cat(3,imgRef_1,imgRef_2,imgRef_3,imgRef_4);%,imgRef_5,imgRef_6,imgRef_7,imgRef_8);

%==========================================================
% Calculate NDVI
%==========================================================
b2 = 4;
b1 = 3;


NDVI_1    = (imgRef_1(:,:,b2) - imgRef_1(:,:,b1)) ./ (imgRef_1(:,:,b2) + imgRef_1(:,:,b1));                 % clear image 1
NDVI_2    = (imgRef_2(:,:,b2) - imgRef_2(:,:,b1)) ./ (imgRef_2(:,:,b2) + imgRef_2(:,:,b1));                 % clear image 2
NDVI_3    = (imgRef_3(:,:,b2) - imgRef_3(:,:,b1)) ./ (imgRef_3(:,:,b2) + imgRef_3(:,:,b1));                 % clear image 3
NDVI_4    = (imgRef_4(:,:,b2) - imgRef_4(:,:,b1)) ./ (imgRef_4(:,:,b2) + imgRef_4(:,:,b1));                 % clear image 4
% NDVI_5    = single(imread('Images\NDVI-tiff\S2A_L2A_20170111T100351_N0204_R122_T33UXQ_20170111T100351_NDVI_B0804.tif'));                   % clear image 4
% NDVI_6    = single(imread('Images\NDVI-tiff\S2A_L2A_20170329T095021_N0204_R079_T33UXQ_20170329T095024_NDVI_B0804.tif'));
% NDVI_7    = single(imread('Images\NDVI-tiff\S2A_L2A_20170401T100021_N0204_R122_T33UXQ_20170401T100022_NDVI_B0804.tif'));% clear image 4
% NDVI_8    = single(imread('Images\NDVI-tiff\S2A_L2A_20170511T100031_N0205_R122_T33UXQ_20170511T100539_NDVI_B0804.tif'));% clear image 4

NDVI_ts = cat(3, NDVI_1, NDVI_2, NDVI_3, NDVI_4);%, NDVI_5, NDVI_6, NDVI_7, NDVI_8 );
%
NDVI_ts = NDVI_1; cMaskRef = cMaskRef1; imgRef = imgRef_1;
%
NDVI_ts(isnan(NDVI_ts)) = 0;

%==========================================================
% Mask Processing
%==========================================================

% se = strel('disk',50);
% se1 = strel('disk',20);
% cMaskTarget = imdilate(cMaskTarget,se);
% cMaskRef = imdilate(cMaskRef,se1);
cMaskMultiTemp = or( cMaskTarget~=0, cMaskRef~=0 );
imgTarget_est = imgTarget;

%==========================================================
% Restore Image
%==========================================================

nl = 5000;
nc = 5000;

n_tile_x = ceil(size(imgTarget,1)/nl);
n_tile_y = ceil(size(imgTarget,2)/nc);


for i_tile_y =  1% : n_tile_y
      
    for i_tile_x =  1% : n_tile_x
    
            %% Cut Image in tile region

            l1 = nl*(i_tile_x-1)+1;
            c1 = nc*(i_tile_y-1)+1;
            
            Rmax = min( l1+nl-1, size(imgTarget,1) );
            Rmin = max( l1, 1 );
            Cmax = min( c1+nc-1 , size(imgTarget,2) );
            Cmin = max( c1, 1);

            nl_cut = Rmax - Rmin +1 ;
            nc_cut = Cmax - Cmin +1 ;
            nb = size(imgTarget,3);
            
            %% Multittemporal CLustering

            NDVI_ts_Vec    = reshape( NDVI_ts( Rmin:Rmax, Cmin:Cmax, : ) , [nl_cut* nc_cut, size(NDVI_ts,3)]) ; 
            cMaskRefVec    = reshape( cMaskRef( Rmin:Rmax, Cmin:Cmax ), [nl_cut* nc_cut, 1] ) ;
            cMaskTargetVec = reshape( cMaskTarget( Rmin:Rmax, Cmin:Cmax ), [nl_cut* nc_cut, 1] ) ;
%             pool = parpool;
%             stream = RandStream('mlfg6331_64');  % Random number stream
%             options = statset('UseParallel',1,'UseSubstreams',1,...
%              'Streams',stream);
%             [ label1,~ ]   = kmeans( NDVI_ts_Vec( cMaskRefVec==0,:), k_Region, 'Distance', 'sqeuclidean','Options',options, 'MaxIter',100,'Display','final' );  
%             x = linspace(-1,1,k_Region);
% %             [~,ind1] = min(abs(bsxfun(@minus,NDVI_ts_Vec( cMaskRefVec==0,:)',x.'))); %//' Find the indices of the
% %             output = x(ind1);
% %             c = unique(output);
% %             for i = 1 : k_Region
% %                 output(output==c(i)) = i;
% %             end
% %             label1 = output';
%             label1 = discretize(NDVI_ts_Vec( cMaskRefVec==0,:),x);
            label1 = discretize(NDVI_ts_Vec( cMaskRefVec==0,:),k_Region);
            label1 = kmeans(label1,k_Region);
%             label1 = mode(label1,2);

            LabelKmean = zeros(size(cMaskRefVec,1),1);
            LabelKmean(cMaskRefVec == 0) = label1;
            labelclouds = LabelKmean( cMaskTargetVec == 1 );
            counter = unique(labelclouds);
            
            %% Convert to Vector
            multiMaskTempVec  = reshape( cMaskMultiTemp( Rmin:Rmax, Cmin:Cmax ) , [nl_cut* nc_cut, 1]) ; 
            TargetVec         = reshape( imgTarget   ( Rmin:Rmax , Cmin:Cmax , : ),    [nl_cut*nc_cut,nb] ); 
            imgRefVec         = reshape( imgRef( Rmin:Rmax , Cmin:Cmax , : ), [nl_cut*nc_cut,size(imgRef,3)] );             
            PixUnderCloudRef  = imgRefVec(( cMaskTargetVec == 1 ),: );         
            PixUnderCloudTarg = TargetVec((cMaskTargetVec == 1),:);
            NDVI_ts_UnderCloudRef = NDVI_ts_Vec(( cMaskTargetVec == 1 ),: );   
            
            
             for i_cluster = 1 : size(counter,1)
                    disp(i_cluster);
                    samples_under_cloud_cluster = PixUnderCloudRef( labelclouds==counter(i_cluster), : );
                    sel = LabelKmean == counter(i_cluster);
                    sel(multiMaskTempVec) = 0;
                    
                    if sum(unique(sel)) ~= 0
                        img_t2_label_sel = imgRefVec(sel,:);
                        img_t1_label_sel = TargetVec(sel,:); 
       
                        n = knnsearch( NDVI_ts_Vec(sel,:) , NDVI_ts_UnderCloudRef(labelclouds==counter(i_cluster),:), 'K', 1 ); %ref
                        n = unique(n);
                        
                        n = knnsearch( img_t1_label_sel , img_t1_label_sel(n,:), 'K', 40 ); %ref
                        n = unique(n);
                        idx = randsample(size(n,1),min(size(n,1),2000));
                      
                        inputs  = img_t2_label_sel( n(idx), : );
                        targets = img_t1_label_sel( n(idx), : );

                        for bb = 1:nb
                             
%                             X = inputs;
%                             Y = targets(:,bb);
%                             Mdl = TreeBagger(50,X,Y,'OOBPrediction','On','Method','regression');
%                             predicted_label = predict(Mdl,samples_under_cloud_cluster);
                            
                            b = regress(targets(:,bb),[ones(size(inputs,1),1) inputs inputs.^2 ]);
                            predicted_label = b'*[ones(size(samples_under_cloud_cluster,1),1) samples_under_cloud_cluster samples_under_cloud_cluster.^2  ]';
                            predicted_label = predicted_label';
                            PixUnderCloudTarg( labelclouds==counter(i_cluster),bb) = predicted_label;
                            
                        end
                    end
             end
            TargetVec(cMaskTargetVec==1,:) = PixUnderCloudTarg;
            imgTarget_est( Rmin:Rmax, Cmin:Cmax, :)  = reshape(TargetVec, [nl_cut,nc_cut,nb]);

    end
end
% [imgTarget, R] = geotiffread('InputDataset_2015_2016\M_L8_t2.tif'); % cloudy image
% geoInfo        = geotiffinfo('InputDataset_2015_2016\M_L8_t2.tif');
% CoordSyst      = ['EPSG:' num2str(geoInfo.GeoTIFFTags.GeoKeyDirectoryTag.ProjectedCSTypeGeoKey)];
% 
% geotiffwrite('out\M_L8_t2_no_clouds.tif', single(imgTarget_est), R,'CoordRefSysCode', CoordSyst);



%% Show Results
imgTarget = single(imgTarget);
rgb  = cat(3, imgTarget(:,:,3), imgTarget(:,:,2) ,imgTarget(:,:,1));
rgb2 = cat(3, imgRef(:,:,3), imgRef(:,:,2) ,imgRef(:,:,1));
rgb3 = cat(3, imgTarget_est(:,:,3), imgTarget_est(:,:,2) ,imgTarget_est(:,:,1));

%% Plot
 edge = imdilate(xor(imdilate(cMaskTarget,strel('disk',7)),imerode(imdilate(cMaskTarget,strel('disk',7)),strel('disk',1))),strel('disk',1));

            for b=1:3
                app  = rgb3(:,:,b);
                app2 = rgb (:,:,b);
                if b==1
                    app(edge)  = 1e4;
                    app2(edge) = 1e4;
                else 
                    app(edge)  = 0;
                    app2(edge) = 0;
                end
                rgb3(:,:,b) = app;
                rgb (:,:,b) = app2;
            end
            
figure(),
ax1 = subplot(1,2,1); imshow(rgb3(1:Rmax,1:Cmax,:)*3/1e4);title('BagTree');
ax2 = subplot(1,2,2); imshow(rgb(1:Rmax,1:Cmax,:)*3/1e4);
linkaxes([ax1,ax2]);
%             segMap = reshape(LabelKmean,[2000 , 2500]);
%             figure();
%             ax1 = subplot(2,3,1); imshow(rgb3(1:Rmax,1:Cmax,:)*6/1e4)
%             ax2 = subplot(2,3,2); imshow(imgRef_2(:, :,[3 2 1])*4/1e4)
%             ax3 = subplot(2,3,3); imshow(imgRef_3(:, :,[3 2 1])*4/1e4)
%             ax4 = subplot(2,3,4);imshow(imgRef_4(:, :,[3 2 1])*4/1e4)
%             ax5 = subplot(2,3,5); imshow(imgTarget(:, :,[3 2 1])*6/1e4)
%             ax6 = subplot(2,3,6); imagesc(segMap), daspect([1 1 1 ])
%             linkaxes([ax1,ax2,ax3,ax4,ax5,ax6]);
% 
