clc;clear; close all;

% InputPathImages = 'D:\GiulioWeikmann\Sentinel-2\Processed\Images\';
% InputPathMasks  = 'D:\GiulioWeikmann\Sentinel-2\Processed\CloudMask\';
% 
% S2_img   = dir([InputPathImages '*.tif']);
% S2_masks = dir([InputPathMasks '*.tif']);
% 
% counter = zeros(size(S2_img,1),1);
% 
% for i = 1:size(S2_img,1)
%     disp(i);
%     mask = imread([InputPathMasks S2_masks(i).name]);
%     mask(mask==1) = 0;
%     mask(or(or(or(mask==8,mask==7),mask==9),mask==10)) = 1;
%     mask(mask~=1) = 0;
%     mask = mask == 1;
%     if sum(sum(mask)) < size(mask,1)*size(mask,2)*0.10
%         counter(i) = 1;
%     end
% end
% save('D:\GiulioWeikmann\Sentinel-2\Clear','counter');


listM = dir(['D:\GiulioWeikmann\RestorationCode\Output\Masks\' '*mat']);
listI = dir(['D:\GiulioWeikmann\Sentinel-2\Processed\Images\' '*tif']);

clearImmInd = importdata('D:\GiulioWeikmann\Sentinel-2\ClearImmInd.mat');
ImmInd = find(clearImmInd==0);
ImmInd = ImmInd(1:size(listM,1));
Rmax = 5000; Cmax = 5000;
for i = 17:30
    imgTarget = imread([listI(ImmInd(i)).folder '\' listI(ImmInd(i)).name]);
    v = importdata(['D:\GiulioWeikmann\RestorationCode\Output\Ensemble\' num2str(ImmInd(i)) '.mat']);
%     v = importdata(['D:\GiulioWeikmann\RestorationCode\Output\NewIm\' num2str(ImmInd(i)) '.mat']);
    mask = importdata(['D:\GiulioWeikmann\RestorationCode\Output\Masks\' num2str(ImmInd(i)) '.mat']);
    
    edge = imdilate(xor(imdilate(mask,strel('disk',7)),imerode(imdilate(mask,strel('disk',7)),strel('disk',1))),strel('disk',1));

    for b=1:3
        app  = imgTarget(:,:,b);
%         app2 = rgb (:,:,b);
        if b==3
            app(edge)  = 1e4;
%             app2(edge) = 1e4;
        else 
            app(edge)  = 0;
%             app2(edge) = 0;
        end
        imgTarget(:,:,b) = app;
%         rgb (:,:,b) = app2;
    end

    figure(),
    ax1 = subplot(1,2,1); imshow(v(1:Rmax,1:Cmax,:)*3/1e4);title('Regression');
    ax2 = subplot(1,2,2); imshow(imgTarget(1:Rmax,1:Cmax,[3,2,1])*3/1e4);
    linkaxes([ax1,ax2]);

    
%     figure(),
%     ax1 = subplot(1,2,1); imshow(v(1:Rmax,1:Cmax,:)*3/1e4);title('BagTree');
%     ax2 = subplot(1,2,2); imshow(imgTarget(1:Rmax,1:Cmax,[3,2,1])*3/1e4);
%     linkaxes([ax1,ax2]);
end