clc;clear;close all;

listM = dir(['D:\GiulioWeikmann\RestorationCode\Output\Masks\' '*mat']);
listI = dir(['D:\GiulioWeikmann\Sentinel-2\Processed\Images\' '*tif']);

listMS2C = dir(['D:\GiulioWeikmann\Sentinel-2\Processed\CloudMask\' '*.tif']);


clearImmInd = importdata('D:\GiulioWeikmann\Sentinel-2\ClearImmInd.mat');
ImmInd = find(clearImmInd==0);
ImmInd = ImmInd(1:size(listM,1));

ImmRef = zeros(size(listM,1),1);

for i = 16:27%size(listM)
    disp(i);
    img = imread([listI(ImmInd(i)).folder '\' listI(ImmInd(i)).name]);
    mask = importdata([listM(i).folder '\' num2str(ImmInd(i)) '.mat']);
    if sum(sum(mask)) < size(mask,1)*size(mask,2)*0.15
        ImmRef(i) = 1;
    end
%     figure(),imshow(img(:,:,[3,2,1])*2/1e4 .* (~mask));

    S2CM = imread([listMS2C(ImmInd(i)).folder '\' listMS2C(ImmInd(i)).name]);
    S2CM = and(S2CM>=7,S2CM<11);

    figure(),
    ax1 = subplot(1,2,1); imshow(imoverlay(img(:,:,[3,2,1])*2/1e4,mask,'red'));title('NewMask');
    ax2 = subplot(1,2,2); imshow(imoverlay(img(:,:,[3,2,1])*2/1e4,S2CM,'red'));title('Sen2Cor');
    
%     ax1 = subplot(1,2,1); imshow(img(:,:,[3,2,1])*2/1e4 .* (~mask));title('NewMask');
%     ax2 = subplot(1,2,2); imshow(img(:,:,[3,2,1])*2/1e4 .* (~S2CM));title('Sen2Cor');
    linkaxes([ax1,ax2]);
 
end
% tile


% save('D:\GiulioWeikmann\Sentinel-2\ImmRef.mat', 'ImmRef');