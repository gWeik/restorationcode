function [a,blueimm] = blueClouds(imm,ind)

    imm = imm*4/1e4;
    copyImm = imm(:,:,1);
    copyImm(ind) = 1;
    imm(:,:,1) = copyImm;
    blueimm = imm;
    a = figure();imshow(imm(:,:,[3,2,1]));

end

