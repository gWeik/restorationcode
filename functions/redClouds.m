function [a, img] = redClouds(imm,ind)

    imm = imm*4/1e4;
    copyImm = imm(:,:,3);
    copyImm(ind) = 1;
    imm(:,:,3) = copyImm;
    img = imm;
    a = figure();imshow(imm(:,:,[3,2,1]));

end

