function [ transform_components ] = wavelet_transform( img , level)
   
   [c,s]=wavedec2(img, 3,'haar');

   [H1,V1,D1] = detcoef2('all',c,s,level);
   A1 = appcoef2(c,s,'haar',level);
%    Vimg = wcodemat(V1,255,'mat',1); % vertical component
%    Himg = wcodemat(H1,255,'mat',1); % horizontal component
%    Dimg = wcodemat(D1,255,'mat',1); % diagonal component
   Aimg = wcodemat(A1,255,'mat',1); % rescaled image
   
%    transform_components = {Aimg, Himg, Vimg, Dimg};
   transform_components = {Aimg, abs(H1), abs(V1), abs(D1)};
   
   

end

