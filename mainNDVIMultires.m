clc;clear;close all;

% InputPathImages = 'D:\GiulioWeikmann\Sentinel-2/Images\';
InputPathImages = 'D:\GiulioWeikmann\Sentinel-2\Processed\Images\';
InputPathMasks  = 'D:\GiulioWeikmann\Sentinel-2\Processed\CloudMask\';

addpath('functions');

S2_img   = dir([InputPathImages '*.tif']);
S2_img = S2_img(52:end);
S2_masks = dir([InputPathMasks '*.tif']);
S2_masks = S2_masks(52:end);

%========================================================================
% Import Data
%========================================================================

img = imread([S2_img(1,1).folder '/' S2_img(1,1).name]);
mask = imread([S2_masks(1,1).folder '/' S2_masks(1,1).name]);
[nl,nc,~] = size(img);

clearImmInd = importdata('D:\GiulioWeikmann\Sentinel-2\ClearImmInd.mat');
clearImmInd = clearImmInd(52:end);

n_imm = 20; %size(S2_img,1)
clearImmInd = clearImmInd(1:n_imm);
% CloudImages = find( clearImmInd == 1 );
ImmInd = find(clearImmInd==0);

TS_B    = zeros( nl*nc, size(ImmInd,1) );
TS_G    = zeros( nl*nc, size(ImmInd,1) );
TS_R    = zeros( nl*nc, size(ImmInd,1) );
TS_11   = zeros( nl*nc, size(ImmInd,1) );
TS_12   = zeros( nl*nc, size(ImmInd,1) );
TS_IR   = zeros( nl*nc, size(ImmInd,1) );
% Haze    = zeros( nl*nc, 10 );

TS_masks    = zeros( nl*nc, size(ImmInd,1) );

TS_B(:,1)    = reshape( img(:,:,1), [nl*nc,1]);
TS_G(:,1)    = reshape( img(:,:,2), [nl*nc,1]);
TS_R(:,1)    = reshape( img(:,:,3), [nl*nc,1]);
TS_IR(:,1)   = reshape( img(:,:,4), [nl*nc,1]);
TS_11(:,1)   = reshape( img(:,:,9), [nl*nc,1]);
TS_12(:,1)   = reshape( img(:,:,10), [nl*nc,1]);
% Haze(:,1)    = reshape( HazeDetector_f(img(:,:,[1,2,3])), [nl*nc,1]);

TS_masks(:,1)    = reshape( mask, [nl*nc,1]);

    for i = 2:10%size(ImmInd,1)    %: n_imm %size(S2_img,1)
%         figure,imshow(img(:,:,[3 2 1])*4/1e4)
        disp(i);
        img = imread([S2_img(ImmInd(i),1).folder '/' S2_img(ImmInd(i),1).name]);   
        TS_B(:,i)    = reshape( img(:,:,1), [nl*nc,1]);
        TS_G(:,i)    = reshape( img(:,:,2), [nl*nc,1]);
        TS_R(:,i)    = reshape( img(:,:,3), [nl*nc,1]);
        TS_IR(:,i)   = reshape( img(:,:,4), [nl*nc,1]);
        TS_11(:,i)   = reshape( img(:,:,9), [nl*nc,1]);
        TS_12(:,i)   = reshape( img(:,:,10), [nl*nc,1]);
        mask = imread([S2_masks(ImmInd(i),1).folder '/' S2_masks(ImmInd(i),1).name]);
        TS_masks(:,i) = reshape( mask, [nl*nc,1]);
%         if i < 10
%             Haze(:,i)     = reshape( HazeDetector_f(img(:,:,[1,2,3])), [nl*nc,1]);
%         end
        
    end
    
% Haze = importdata('D:\GiulioWeikmann\PythonConv\SEOM\data\haze.mat');    
% tile    
clear img mask 

res = {[nl,nc],[nl/2,nc/2]};
n_res = 2;

%%RESIZE
for i = 1:10%size(ImmInd,1)
    TS_Br(:,i) = reshape(imresize(reshape(TS_B(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
    TS_Gr(:,i) = reshape(imresize(reshape(TS_G(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
    TS_Rr(:,i) = reshape(imresize(reshape(TS_R(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
    TS_IRr(:,i) = reshape(imresize(reshape(TS_IR(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
    TS_11r(:,i) = reshape(imresize(reshape(TS_11(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
    TS_12r(:,i) = reshape(imresize(reshape(TS_12(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
    TS_masksr(:,i) = reshape(imresize(reshape(TS_masks(:,i),res{1}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
%     if i < 11
%         Hazer(:,i) = reshape(imresize(reshape(Haze(:,i),res{n_res}),1/n_res),[res{n_res}(1)*res{n_res}(2),1]);
%     end
end

%========================================================================
% Plot Haze
%========================================================================
% for i = 1:10
%     img = cat(3,reshape(TS_R(:,i),[5004,5010]),reshape(TS_G(:,i),[5004,5010]),reshape(TS_B(:,i),[5004,5010]));
%     figure(i),imshow(img(:,:,[1 2 3])*4/1e4 .* (reshape(Haze(:,i),[5004,5010])>quantile(Haze(:,i),0.9)));
% end
% tile

%========================================================================
% Temporal Trend Analysis
%========================================================================

TS_NDVIr = (TS_IRr - TS_Rr) ./ (TS_IRr + TS_Rr);
% for i = 1:10
%    figure(), imagesc(reshape(TS_NDVIr(:,i),[nl/2,nc/2])); daspect([1,1,1]);
% end
% tile

cloud_Th = quantile(TS_Br,0.65,2);
Mask_New = zeros(size(TS_Br));
Mask_New(TS_Br>cloud_Th)=1;

% Q = 0.4;
% for i = 1:2
%     i_m = 1 + 5*(i-1);
%     var_Th = var(TS_Br(:,i_m:i_m+4),1,2);
%     
%     Mask_New_5 = Mask_New(:,i_m:i_m+4);
%     Mask_New_5(var_Th<quantile(var_Th,Q),:) = 0;
%     Mask_New(:,i_m:i_m+4) = Mask_New_5;
% end


Q = 0.4;
for i = 1:2
    i_m = 1 + 5*(i-1);
    var_Th = var(TS_NDVIr(:,i_m:i_m+4),1,2);
    
    Mask_New_5 = Mask_New(:,i_m:i_m+4);
    Mask_New_5(var_Th<quantile(var_Th,Q),:) = 0;
    Mask_New(:,i_m:i_m+4) = Mask_New_5;
end


%========================================================================
% Plot Variance
% ========================================================================
varrr{1} = var(TS_NDVIr(:,1:5),1,2);
varrr{2} = var(TS_NDVIr(:,6:10),1,2);
% ivar = 1;
% for i = 1 : size(Mask_New,2)
%     if i > 5
%         ivar = 2;
%     end
%     img = cat(3,reshape(TS_Rr(:,i),res{n_res}),reshape(TS_Gr(:,i),res{n_res}),reshape(TS_Br(:,i),res{n_res}));
%     figure(i), imshow(img*4/1e4 .* reshape(varrr{ivar}<quantile(varrr{ivar},Q),res{n_res}));
% end
% tile
% close all;

% for i = 1 : 10%size(Mask_New,2)
%     img = cat(3,reshape(TS_Rr(:,i),[nl/2,nc/2]),reshape(TS_Gr(:,i),[nl/2,nc/2]),reshape(TS_Br(:,i),[nl/2,nc/2]));
%     figure(i), imshow(img*4/1e4 .* reshape(Mask_New(:,i),[nl/2,nc/2]));
% end
% tile


for i = 1 : size(Mask_New,2)
    Mask_New(:,i) = reshape(bwareaopen(reshape(Mask_New(:,i),res{n_res}),50),[res{n_res}(1)*res{n_res}(2),1]);
end



%% CLEAR PIXEL
% cloud_Th = quantile(TS_Br,0.5,2);
% clearP = zeros(size(TS_Br));
% clearP(TS_Br<cloud_Th)=1;
% Q = 0.4;
% for i = 1:2
%     i_m = 1 + 5*(i-1);
%     var_Th = var(TS_NDVIr(:,i_m:i_m+4),1,2);
%     
%     clearP_5 = clearP(:,i_m:i_m+4);
%     clearP_5(var_Th<quantile(var_Th,Q),:) = 1;
%     Mask_New(:,i_m:i_m+4) = Mask_New_5;
% end
% 
ClearPix = [];
for i = 1 : 2
    m = reshape(varrr{i}<quantile(varrr{i},0.3),res{n_res});
%     m = imerode(m,strel('disk',3));
    ClearPix = [ClearPix, repmat(reshape(m,[res{n_res}(1)*res{n_res}(2),1]),1,5)];
end

% for i = 1 : 10%size(Mask_New,2)
%     img = cat(3,reshape(TS_Rr(:,i),[nl/2,nc/2]),reshape(TS_Gr(:,i),[nl/2,nc/2]),reshape(TS_Br(:,i),[nl/2,nc/2]));
%     figure(i), imshow(img*4/1e4 .* reshape(ClearPix(:,i),[nl/2,nc/2]));
% end
% tile


    
%========================================================================
% Extract Noisy Training Set (AGGIUNGERE ALTRE BANDE)
%========================================================================
CloudSen2Cor = and(TS_masks>=7,TS_masks<11);
% ClearSen2Cor = and(TS_masks>3,TS_masks<7);
% 
% TS_Br = TS_Br(:,1:10);
% TS_Rr = TS_Rr(:,1:10);
% TS_Gr = TS_Gr(:,1:10);
% TS_IRr = TS_IRr(:,1:10);
% TS_11r = TS_11r(:,1:10);
% TS_12r = TS_12r(:,1:10);
% TS_B = TS_B(:,1:10);
% TS_R = TS_R(:,1:10);
% TS_G = TS_G(:,1:10);
% TS_IR = TS_IR(:,1:10);
% TS_11 = TS_11(:,1:10);
% TS_12 = TS_12(:,1:10);


CloudClass =  [ TS_Br(Mask_New    == 1), ...
                TS_Rr(Mask_New    == 1), ...
                TS_Gr(Mask_New    == 1), ...
                TS_IRr(Mask_New   == 1), ...
                TS_11r(Mask_New   == 1), ...
                TS_12r(Mask_New   == 1)];
ClearClass =  [ TS_Br(ClearPix    == 1), ...
                TS_Rr(ClearPix    == 1), ...
                TS_Gr(ClearPix    == 1), ...
                TS_IRr(ClearPix   == 1), ...
                TS_11r(ClearPix    == 1), ...
                TS_12r(ClearPix    == 1)];   
ShadowClass = [ TS_Br(TS_masksr    == 3), ...
                TS_Rr(TS_masksr    == 3), ...
                TS_Gr(TS_masksr    == 3), ...
                TS_IRr(TS_masksr   == 3), ...
                TS_11r(TS_masksr    == 3), ...
                TS_12r(TS_masksr    == 3)];

idx = randsample( size(CloudClass,1),    min(size(CloudClass,1),    1e4 ));
TR{1,1} = CloudClass(idx,:);

idx = randsample( size(ClearClass,1),    min(size(ClearClass,1),    1e4 ));
TR{2,1} = ClearClass(idx,:);

idx = randsample( size(ShadowClass,1),   min(size(ShadowClass,1),   1e4 ));
TR{3,1} = ShadowClass(idx,:);

%========================================================================
% Extract Core of the Distribution
%========================================================================
classDistribution = cell(3,1);
alpha = 0.9; % lower values  more conservative

    for iii = 1:3
        
        omega = TR{iii,1};
%             d2= 1;
%             x=0;
%             while sum(d2>x) ~=0  
%                 gm = fitgmdist(omega,1,'RegularizationValue',0.1);
%                 d2 = mahal(gm,omega);
%                 x = chi2inv( alpha , 2*size(omega,2) );
%                 omega(d2>x,:)=[];
%             end
%             label = ones( size( omega,1 ) ,1) * iii;
        classDistribution{iii,1} =   omega ;

    end

%========================================================================
% Ensemble
%========================================================================

X = [classDistribution{1,1};classDistribution{2,1};classDistribution{3,1}];
Y = [ones(size(classDistribution{1,1},1),1);ones(size(classDistribution{2,1},1),1)*2;ones(size(classDistribution{3,1},1),1)*3];

Mdl  = fitcensemble(X,Y,'Method','AdaBoostM2','NumLearningCycles',10);

indSel = 2;

[predMeanX,score] = predict(Mdl,[TS_B(:,indSel),TS_R(:,indSel),TS_G(:,indSel),TS_IR(:,indSel),TS_11(:,indSel),TS_12(:,indSel)]);
hscore = (score(1,1) + score(1,2) + score(1,3))/2;
pscore = max(score,[],2);
Pnuvola = pscore<(score(1,1) + score(1,2) + score(1,3))*0.5;
MaskpCloud = reshape(Pnuvola,[nl,nc]);
MaskpCloud = imopen(MaskpCloud,strel('disk',2));


MaskCloud = reshape(predMeanX,[nl,nc]);






% ImmInd = find(clearImmInd==0);
Immind = ImmInd(indSel);

img = imread([S2_img(Immind,1).folder '/' S2_img(Immind,1).name]); 
rgb = img(:,:,[3 2 1])*4/1e4;
rgb_masked = rgb.*(MaskCloud~=1);
% [~,blueimm] = blueClouds(img,find(MaskCloud==1)); title(['VarQuantile' num2str(Q)]);
% redClouds(img,find(MaskCloud==1)), title(['VarQuantile' num2str(Q)]);

close all;

% figure(),
% ax1 = subplot(1,2,1); imshow(rgb);
% ax2 = subplot(1,2,2);imagesc(rgb_masked),daspect([1,1,1]);title('NewMask');
% linkaxes([ax1,ax2]);


figure(),imagesc(rgb_masked),daspect([1,1,1]);title('NewMask');
figure(), imagesc(rgb.*(reshape(CloudSen2Cor(:,3),[nl,nc])==0)),daspect([1,1,1]);daspect([1,1,1]);title('Sen2Cor');


Hop = MaskCloud==1;
Hop = imopen(Hop==1,strel('disk',3));
Hop = bwareaopen(Hop,200);
rgb_m = rgb .* ~(or(Hop==1,and(imdilate(Hop==1,strel('disk',10)),reshape(CloudSen2Cor(:,3),[nl,nc])==1)));
figure(), imagesc(rgb_m);daspect([1,1,1]);



Mcloud = MaskCloud == 1;
Mcloud = bwareaopen(Mcloud,200);
Mcloud = imdilate(Mcloud,strel('disk',3));
Mcloud(and(Mcloud == 1, MaskCloud ==4)) = 1;
figure(),imshow(img(:,:,[3 2 1])*4/1e4 .* (Mcloud~=1));




figure(), imagesc(rgb .* (~MaskpCloud));daspect([1,1,1]);title('pNuvola');
figure(), imagesc(rgb .* (~MaskpCloud) .* (MaskCloud~=1));daspect([1,1,1]);title('Mask');



%% Cromatic aberrations
Aberr = img(:,:,[3,2,1])/10000;
i_lin = rgb2lin(Aberr);
chart = esfrChart(i_lin,'Sensitivity',1);
displayChart(chart,'displayColorROIs',false,...
    'displayGrayROIs',false,'displayRegistrationPoints',false)


%% HAZE



PROVA = HazeDetector_f(img(:,:,[3,2,1]));
rgb_m = rgb.*(PROVA<quantile(PROVA,0.7)).*(MaskCloud~=1);
imshow(rgb_m);
